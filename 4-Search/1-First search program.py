# ----------
# User Instructions:
#
# Define a function, search() that returns a list
# in the form of [optimal path length, row, col]. For
# the grid shown below, your function should output
# [11, 4, 5].
#
# If there is no valid path from the start point
# to the goal, your function should return the string
# 'fail'
# ----------

# Grid format:
#   0 = Navigable space
#   1 = Occupied space

grid = [[0, 0, 1, 0, 0, 0],
        [0, 0, 1, 0, 0, 0],
        [0, 0, 0, 0, 1, 0],
        [0, 0, 1, 1, 1, 0],
        [0, 0, 0, 0, 1, 0]]
init = [0, 0]
goal = [len(grid)-1, len(grid[0])-1]
cost = 1

delta = [[-1, 0], # go up
         [ 0,-1], # go left
         [ 1, 0], # go down
         [ 0, 1]] # go right

delta_name = ['^', '<', 'v', '>']



def search(grid,init,goal,cost):
    open = [[0,init[0],init[1]]]
    current = [0,init[0],init[1]]
    steps = []
    step = 0
    visited = []
    checked = []
    first = 1
    ending = current
    next = []

    while open != [] or ending != goal:
        #test different directions
        step = current[0]
        for direction in delta:
            if current[1] + direction[0] >= 0 and \
                current[1] + direction[0] < len(grid) and \
                current[2] + direction[1] >= 0 and \
                current[2] + direction[1] < len(grid[0]) and \
                [current[1]+direction[0],current[2]+direction[1]] not in checked and \
                [current[1]+direction[0],current[2]+direction[1]] not in visited:

                    value = grid[current[1]+direction[0]][current[2]+direction[1]]
                    if value != 1:
                        open.append([current[0]+cost, current[1] + direction[0], current[2] + direction[1]])
                        checked.append([current[1]+direction[0], current[2] + direction[1]])
                        if first == 1:
                            checked.append([current[1], current[2]])


        #print "open",  open
        #print "checked" , checked
        next_one = 9999
        for check in open:
            if check[0]<next_one:
                next_one = check[0]

        removal = []
        for check in open:
            if check[0] == next_one:
                #found the min. return it
                next = check
                removal = check
        if first != 1 and removal != []:
            open.remove(removal)
        else:
            first = 0
        current = next
        if removal == [] and visited == []:
            break
        try:
            ending = [current[1],current[2]]
        except:
            print "reached the end"
        #print "current", current
        #break
#grid = [[*, *, 1, 0, 0, 0],
#        [+, +, 1, 0, 0, 0],
#        [1, 1, 1, 0, 1, 0],
#        [0, 0, 1, 1, 1, 0],
#        [0, 0, 0, 0, 1, 0]]


        #print "beer"
    if ending == goal:
        path = current
    else:
        path = "fail"

    return path


answer = search(grid,init,goal,cost)
print answer